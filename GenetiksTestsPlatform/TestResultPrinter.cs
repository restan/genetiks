﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Genetiks.Monitor;
using GenetiksTestsPlatform.TesterTestResults;

namespace GenetiksTestsPlatform
{

    public class TestResultPrinter
    {
        public void Print(string path, IList<TestResult> testResults)
        {
            var dateTimeFolderName = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
            var fullPath = Path.Combine(path, dateTimeFolderName);

            if (Directory.Exists(fullPath))
                throw new Exception();

            Directory.CreateDirectory(fullPath);

            PrintResults(fullPath, testResults);
            PrintDetails(fullPath, testResults);
        }


        private void PrintDetails(string path, IEnumerable<TestResult> testResults)
        {
            foreach (var testResult in testResults)
            {
                var lines = new List<string>();

                lines.Add(ResultHeader);
                lines.Add(FormatResultData(testResult));

                lines.Add("");
                lines.Add("");

                lines.AddRange(testResult.TestUnit.Monitor.Data.Select(FormatDetail));

                File.WriteAllLines(Path.Combine(path, "details_" + testResult.TestUnit.Guid.ToString() + ".csv"), lines);
            }
        }

        private string FormatDetail(IslandMonitorData arg)
        {
            return String.Format("{0};{1}", arg.IterationNumber, arg.BestCrhomosomeFitness);
        }

        private void PrintResults(string path, IEnumerable<TestResult> testResults)
        {
            var lines = new List<string>();
            lines.Add(ResultHeader);
            lines.AddRange(testResults.Select(FormatResultData));
            File.WriteAllLines(Path.Combine(path, "results.csv"), lines);
        }

        private string ResultHeader
        {
            get
            {
                return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
                                     "guid",
                                     "mintime", "avgtime", "maxtime",
                                     "minresult", "avgresult", "maxresult",
                                     "islands number", "iterations",
                                     "migration freq", "strategy", "population size", "island population size");
            }
        }
        private string FormatResultData(TestResult result)
        {
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}",
                                result.TestUnit.Guid.ToString(),
                                 result.MinTime,
                                 result.AverageTime,
                                 result.MaxTime,
                                 result.MinResult.ToString("#.#"),
                                 result.AverageResult,
                                 result.MaxResult,
                                 result.TestUnit.IslandsNumber,
                                 result.TestUnit.Iterations,
                                 result.TestUnit.MigrationFrequency,
                                 result.TestUnit.MigrationStrategy,
                                 result.TestUnit.PopulationSize,
                                 result.TestUnit.IslandPopulationSize);
        }


    }
}