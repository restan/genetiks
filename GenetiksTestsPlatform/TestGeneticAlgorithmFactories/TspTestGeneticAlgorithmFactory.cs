﻿using Genetiks;
using GenetiksTestsPlatform.TestUnits;
using TSP;

namespace GenetiksTestsPlatform.TestGeneticAlgorithmFactories
{
    public class TspTestGeneticAlgorithmFactory : ITestGeneticAlgorithmFactory
    {
        public IParallelGeneticAlgorithm Create(TestUnit testUnit)
        {
            var tspTestUnit = (TspTestUnit) testUnit;

            var ancestorChromosome = new TSPChromosome(tspTestUnit.Map);
            var ancestorPopulation = new AncestorPopulation(ancestorChromosome, tspTestUnit.FitnessFunction);
            var alg = new IslandsParallelGeneticAlgorithm(ancestorPopulation, tspTestUnit.MigrationStrategy, tspTestUnit.MigrationFrequency, tspTestUnit.IslandFactory);
            for (var i = 0; i < tspTestUnit.IslandsNumber; i++)
                alg.AddPopulation(tspTestUnit.IslandPopulationSize, tspTestUnit.SelectionMethod, tspTestUnit.CrossoverRate, tspTestUnit.MutationRate);

            return alg;
        }
    }
}