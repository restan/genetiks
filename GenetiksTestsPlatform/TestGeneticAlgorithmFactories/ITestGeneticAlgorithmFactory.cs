﻿using Genetiks;
using GenetiksTestsPlatform.TestUnits;

namespace GenetiksTestsPlatform.TestGeneticAlgorithmFactories
{
    public interface ITestGeneticAlgorithmFactory
    {
        IParallelGeneticAlgorithm Create(TestUnit testUnit);
    }
}