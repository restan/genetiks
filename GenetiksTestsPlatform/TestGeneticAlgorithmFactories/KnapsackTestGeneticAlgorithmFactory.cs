﻿using Genetiks;
using GenetiksTestsPlatform.TestUnits;
using KnapsackProblem;

namespace GenetiksTestsPlatform.TestGeneticAlgorithmFactories
{
    public class KnapsackTestGeneticAlgorithmFactory : ITestGeneticAlgorithmFactory
    {
        public IParallelGeneticAlgorithm Create(TestUnit testUnit)
        {
            var knapsackTestUnit = (KnapsackTestUnit)testUnit;
            var ancestorChromosome = new KnapsackChromosome(knapsackTestUnit.Items, knapsackTestUnit.Knapsack);
            var ancestorPopulation = new AncestorPopulation(ancestorChromosome, knapsackTestUnit.FitnessFunction);
            var alg = new IslandsParallelGeneticAlgorithm(ancestorPopulation, knapsackTestUnit.MigrationStrategy, knapsackTestUnit.MigrationFrequency, knapsackTestUnit.IslandFactory);
            for (var i = 0; i < testUnit.IslandsNumber; i++)
                alg.AddPopulation(testUnit.IslandPopulationSize, testUnit.SelectionMethod, testUnit.CrossoverRate, testUnit.MutationRate);

            return alg;
        }
    }
}