﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Genetiks.Factories.SelectionMethods;
using Genetiks.MigrationStrategies.Factory;
using GenetiksTestsPlatform.FileConfiguration;
using GenetiksTestsPlatform.TestGeneticAlgorithmFactories;
using GenetiksTestsPlatform.TestUnits;
using GenetiksTestsPlatform.TestUnits.Factories;
using GenetiksTestsPlatform.TesterTestResults;
using TSP;
using TSP.Map;

namespace GenetiksTestsPlatform.Testers
{
    public class TspTester : ITester
    {
        private readonly ITestGeneticAlgorithmFactory _testGeneticAlgorithmFactory = new TspTestGeneticAlgorithmFactory();
        private readonly MapPersister _mapPersister = new MapPersister();
        private int _testUnitsNumber;
        private int _iterator = 0;
        private int _sameTestsRepeatCount = 10;

        public IEnumerable<TestResult> Test(Configuration configuration)
        {
            var selectionMethodFactory = new SelectionMethodFactory();
            var migrationStrategyFactory = new MigrationStrategyFactory();

            var map = _mapPersister.LoadMap(configuration.DataFile);

            var testsUnitFactory = new TspTestsUnitFactory()
                {
                    Map = map,
                    FitnessFunction = new TSPFitnessFunction(map),
                    CrossoverRates = configuration.CrossoverRates,
                    MutationRates = configuration.MutationRates,
                    IslandsNumbers = configuration.IslandNumbers,
                    Iterations = configuration.Iterations,
                    MigrationFrequencies = configuration.MigrationFrequencies,
                    PopulationSizes = configuration.PopulationSizes,
                    SelectionMethods = configuration.SelectionMethods.Select(selectionMethodFactory.Create),
                    MigrationStrategies = configuration.MigrationStrategies.Select(migrationStrategyFactory.Create)
                };

            _sameTestsRepeatCount = configuration.Repetitions;

            var testUnits = testsUnitFactory.CreateTestUnits().ToList();
            _testUnitsNumber = testUnits.Count;

            return testUnits.Select(Test);
        }

        private TspTestResult Test(TspTestUnit tspTestUnit)
        {
            var alg = _testGeneticAlgorithmFactory.Create(tspTestUnit);

            var times = new List<long>();
            var results = new List<double>();
            for (var i = 0; i < _sameTestsRepeatCount; i++)
            {
                var sw = new Stopwatch();
                sw.Start();
                alg.Compute(tspTestUnit.Iterations);
                sw.Stop();
                times.Add(sw.ElapsedMilliseconds);
                results.Add(((TSPFitnessFunction)tspTestUnit.FitnessFunction).PathLength(alg.BestChromosome));
            }

            Console.WriteLine("{0}/{1}", ++_iterator, _testUnitsNumber);

            return new TspTestResult
            {
                Times = times,
                Results = results,
                TspTestUnit = tspTestUnit
            };

        }
    }
}