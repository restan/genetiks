﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Genetiks.Factories.IslandFactories;
using Genetiks.Factories.SelectionMethods;
using Genetiks.MigrationStrategies.Factory;
using GenetiksTestsPlatform.FileConfiguration;
using GenetiksTestsPlatform.TestGeneticAlgorithmFactories;
using GenetiksTestsPlatform.TestUnits;
using GenetiksTestsPlatform.TestUnits.Factories;
using GenetiksTestsPlatform.TesterTestResults;
using KnapsackProblem;
using KnapsackProblem.DataFileParsers;

namespace GenetiksTestsPlatform.Testers
{
    public class KnapsackTester : ITester
    {
        private readonly ITestGeneticAlgorithmFactory _testGeneticAlgorithmFactory = new KnapsackTestGeneticAlgorithmFactory();
        private readonly KnapsackDataFileParser _dataFileParser = new KnapsackDataFileParser();
        private int _testUnitsNumber;
        private int _iterator = 0;
        private int _sameTestsRepeatCount = 10;

        public IEnumerable<TestResult> Test(Configuration configuration)
        {
            var selectionMethodFactory = new SelectionMethodFactory();
            var migrationStrategyFactory = new MigrationStrategyFactory();
            var islandFactoryFactory = new IslandFactoryFactory();


            var data = _dataFileParser.ParseFile(configuration.DataFile);

            var knapsack = new Knapsack { Capacity = data.Capacity };
            var testsUnitFactory = new KnapsackTestsUnitFactory()
                {
                    Items = data.Items,
                    Knapsack = knapsack,
                    FitnessFunction = new KnapsackFitnessFunction(knapsack, data.Items),
                    CrossoverRates = configuration.CrossoverRates,
                    MutationRates = configuration.MutationRates,
                    IslandsNumbers = configuration.IslandNumbers,
                    Iterations = configuration.Iterations,
                    MigrationFrequencies = configuration.MigrationFrequencies,
                    PopulationSizes = configuration.PopulationSizes,
                    SelectionMethods = configuration.SelectionMethods.Select(selectionMethodFactory.Create),
                    MigrationStrategies = configuration.MigrationStrategies.Select(migrationStrategyFactory.Create),
                    IslandFactories = islandFactoryFactory.Create(configuration.MigrationSelectionStrategies, configuration.RandomFactors)
                };

            _sameTestsRepeatCount = configuration.Repetitions;

            var testUnits = testsUnitFactory.CreateTestUnits().ToList();
            _testUnitsNumber = testUnits.Count;

            return testUnits.Select(Test);
        }

        private TestResult Test(KnapsackTestUnit testUnit)
        {

            var times = new List<long>();
            var results = new List<double>();
            for (var i = 0; i < _sameTestsRepeatCount; i++)
            {
                var alg = _testGeneticAlgorithmFactory.Create(testUnit);
                var sw = new Stopwatch();
                sw.Start();
                alg.Compute(testUnit.Iterations,testUnit.Monitor);
                sw.Stop();
                times.Add(sw.ElapsedMilliseconds);
                results.Add(((KnapsackFitnessFunction)testUnit.FitnessFunction).CalculateItemsValue(alg.BestChromosome));
            }

            Console.WriteLine("{0}/{1}", ++_iterator, _testUnitsNumber);

            return new KnapsackTestResult
                {
                    Times = times,
                    Results = results,
                    KnapsackTestUnit = testUnit
                };

        }
    }
}