﻿using System.Collections.Generic;
using GenetiksTestsPlatform.FileConfiguration;
using GenetiksTestsPlatform.TesterTestResults;

namespace GenetiksTestsPlatform.Testers
{
    public interface ITester
    {
        IEnumerable<TestResult> Test(Configuration configuration);
    }
}