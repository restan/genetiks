﻿using System.Collections.Generic;
using System.Linq;
using GenetiksTestsPlatform.TestUnits;

namespace GenetiksTestsPlatform.TesterTestResults
{
    public abstract class TestResult
    {
        public List<long> Times { get; set; }
        public List<double> Results { get; set; }
        public abstract TestUnit TestUnit { get;}

        public long AverageTime
        {
            get { return (long)Times.Average(); }
        }

        public long MinTime
        {
            get
            {
                return Times.Min();
            }
        }

        public long MaxTime
        {
            get
            {
                return Times.Max();
            }
        }

        public double AverageResult
        {
            get { return Results.Average(); }
        }

        public double MinResult
        {
            get
            {
                return Results.Min();
            }
        }

        public double MaxResult
        {
            get
            {
                return Results.Max();
            }
        }


    }
}