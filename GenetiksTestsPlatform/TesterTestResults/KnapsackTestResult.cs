using GenetiksTestsPlatform.TestUnits;

namespace GenetiksTestsPlatform.TesterTestResults
{
    public class KnapsackTestResult : TestResult
    {
        public override TestUnit TestUnit
        {
            get { return KnapsackTestUnit; }
        }

        public KnapsackTestUnit KnapsackTestUnit { get; set; }

    }
}