﻿using GenetiksTestsPlatform.TestUnits;

namespace GenetiksTestsPlatform.TesterTestResults
{
    public class TspTestResult : TestResult
    {
        public TspTestUnit TspTestUnit { get; set; }

        public override TestUnit TestUnit
        {
            get { return TspTestUnit; }
        }
    }

}