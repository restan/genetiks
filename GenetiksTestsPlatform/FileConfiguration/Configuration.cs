﻿using System.Collections.Generic;
using Genetiks.Factories.IslandFactories;
using Genetiks.Factories.SelectionMethods;
using Genetiks.MigrationStrategies;

namespace GenetiksTestsPlatform.FileConfiguration
{
    public class Configuration
    {
        public IEnumerable<ESelectionMethod> SelectionMethods { get; set; }
        public IEnumerable<double> CrossoverRates { get; set; }
        public IEnumerable<double> MutationRates { get; set; }

        public IEnumerable<EMigrationStrategy> MigrationStrategies { get; set; }
        public IEnumerable<int> MigrationFrequencies { get; set; }
        public IEnumerable<int> IslandNumbers { get; set; }
        public IEnumerable<EIslandFactory> MigrationSelectionStrategies { get; set; }
        public IEnumerable<double> RandomFactors { get; set; }

        public IEnumerable<int> PopulationSizes { get; set; }

        public IEnumerable<int> Iterations { get; set; }
        public int Repetitions { get; set; }

        public string DataFile { get; set; }
    }
}