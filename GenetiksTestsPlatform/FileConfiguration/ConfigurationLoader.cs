﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Genetiks.Factories.SelectionMethods;
using Genetiks.MigrationStrategies;

namespace GenetiksTestsPlatform.FileConfiguration
{
    public class ConfigurationLoader
    {
        public Configuration GetFromFile(string fileName)
        {
            var xmlSerializer = new XmlSerializer(typeof(RawConfiguration));
            var rawConf = xmlSerializer.Deserialize(new FileStream(fileName, FileMode.Open)) as RawConfiguration;
            return ParseRawConfiguration(rawConf);
        }

        private Configuration ParseRawConfiguration(RawConfiguration rawConf)
        {
            return new Configuration
                {
                    SelectionMethods = Split(rawConf.SelectionMethods).Select(EnumParse<ESelectionMethod>).ToList(),
                    CrossoverRates = Split(rawConf.CrossoverRates).Select(ParseDouble).ToList(),
                    MutationRates = Split(rawConf.MutationRates).Select(ParseDouble).ToList(),
                    MigrationStrategies = Split(rawConf.MigrationStrategies).Select(EnumParse<EMigrationStrategy>).ToList(),
                    IslandNumbers = Split(rawConf.IslandNumbers).Select(int.Parse).ToList(),
                    MigrationFrequencies = Split(rawConf.MigrationFrequencies).Select(int.Parse).ToList(),
                    PopulationSizes = Split(rawConf.PopulationSizes).Select(int.Parse).ToList(),
                    Iterations = Split(rawConf.Iterations).Select(int.Parse).ToList(),
                    DataFile = rawConf.MapFile,
                    Repetitions = rawConf.Repetitions
                };
        }
        private double ParseDouble(string s)
        {
            return double.Parse(s.Replace('.', ','));
        }
        private IEnumerable<string> Split(string s)
        {
            return s.Trim().Split(',').Select(x => x.Trim()).ToList();
        }

        private static T EnumParse<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        [XmlRoot("TestsConfiguration")]
        public class RawConfiguration
        {
            public string SelectionMethods { get; set; }
            public string CrossoverRates { get; set; }
            public string MutationRates { get; set; }
            public string MigrationStrategies { get; set; }
            public string IslandNumbers { get; set; }
            public string MigrationFrequencies { get; set; }
            public string PopulationSizes { get; set; }
            public string Iterations { get; set; }

            public string MapFile { get; set; }
            public int Repetitions { get; set; }
        }
    }
}