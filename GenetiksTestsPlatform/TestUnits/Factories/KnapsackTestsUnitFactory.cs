﻿using System;
using System.Collections.Generic;
using System.Linq;
using Genetiks.Monitor;
using KnapsackProblem;

namespace GenetiksTestsPlatform.TestUnits.Factories
{
    public class KnapsackTestsUnitFactory : TestsUnitFactory
    {
        public IList<IKnapsackItem> Items { get; set; }
        public IKnapsack Knapsack { get; set; }

        public IEnumerable<KnapsackTestUnit> CreateTestUnits()
        {
            return from strategy in MigrationStrategies
                   from iterations in Iterations
                   from populationSize in PopulationSizes
                   from selectionMethod in SelectionMethods
                   from islandsNumber in IslandsNumbers
                   from migrationFrequency in MigrationFrequencies
                   from crossoverRate in CrossoverRates
                   from mutationRate in MutationRates
                   from islandFactory in IslandFactories
                   let guid = Guid.NewGuid()
                   select new KnapsackTestUnit
                       {
                           Guid = guid,
                           Monitor = new IslandsParallelGeneticAlgorithmMonitor(guid),
                           Items = Items,
                           Knapsack = Knapsack,
                           FitnessFunction = FitnessFunction,
                           MigrationStrategy = strategy,
                           Iterations = iterations,
                           PopulationSize = populationSize,
                           SelectionMethod = selectionMethod,
                           IslandsNumber = islandsNumber,
                           MigrationFrequency = migrationFrequency,
                           CrossoverRate = crossoverRate,
                           MutationRate = mutationRate,
                           IslandFactory = islandFactory,
                       };
        }
    }
}