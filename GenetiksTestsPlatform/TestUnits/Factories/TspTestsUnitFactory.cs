﻿using System.Collections.Generic;
using System.Linq;

namespace GenetiksTestsPlatform.TestUnits.Factories
{
    public class TspTestsUnitFactory : TestsUnitFactory
    {
        public double[,] Map { get; set; }
        public IEnumerable<TspTestUnit> CreateTestUnits()
        {
            
            return from strategy in MigrationStrategies
                   from iterations in Iterations
                   from populationSize in PopulationSizes
                   from selectionMethod in SelectionMethods
                   from islandsNumber in IslandsNumbers
                   from migrationFrequency in MigrationFrequencies
                   from crossoverRate in CrossoverRates
                   from mutationRate in MutationRates
                   select new TspTestUnit
                       {
                        
                           Map = Map,
                           FitnessFunction = FitnessFunction,
                           MigrationStrategy = strategy,
                           Iterations = iterations,
                           PopulationSize = populationSize,
                           SelectionMethod = selectionMethod,
                           IslandsNumber = islandsNumber,
                           MigrationFrequency = migrationFrequency,
                           CrossoverRate = crossoverRate,
                           MutationRate = mutationRate
                       };
        }
    }
}
