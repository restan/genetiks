﻿using System.Collections.Generic;
using AForge.Genetic;
using Genetiks.Factories.IslandFactories;
using Genetiks.MigrationStrategies;

namespace GenetiksTestsPlatform.TestUnits.Factories
{
    public abstract class TestsUnitFactory
    {
        public IEnumerable<IMigrationStrategy> MigrationStrategies { get; set; }
        public IEnumerable<int> MigrationFrequencies { get; set; }
        public IEnumerable<IIslandFactory> IslandFactories { get; set; }
        public IEnumerable<int> Iterations { get; set; }
        public IEnumerable<int> PopulationSizes { get; set; }
        public IEnumerable<ISelectionMethod> SelectionMethods { get; set; }
        public IEnumerable<int> IslandsNumbers { get; set; }
        public IEnumerable<double> CrossoverRates { get; set; }
        public IEnumerable<double> MutationRates { get; set; }
        public IFitnessFunction FitnessFunction { get; set; }


    }
}