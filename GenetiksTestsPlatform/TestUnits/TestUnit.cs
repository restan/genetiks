﻿using System;
using AForge.Genetic;
using Genetiks.Factories.IslandFactories;
using Genetiks.MigrationStrategies;
using Genetiks.Monitor;

namespace GenetiksTestsPlatform.TestUnits
{
    public abstract class TestUnit
    {
        public Guid Guid { get; set; }
        public IFitnessFunction FitnessFunction { get; set; }
        public IMigrationStrategy MigrationStrategy { get; set; }
        public int Iterations { get; set; }
        public int PopulationSize { get; set; }
        public int IslandPopulationSize { get { return PopulationSize / IslandsNumber; } }
        public ISelectionMethod SelectionMethod { get; set; }
        public int IslandsNumber { get; set; }
        public int MigrationFrequency { get; set; }
        public double CrossoverRate { get; set; }
        public double MutationRate { get; set; }
        public IIslandFactory IslandFactory { get; set; }
        
        public IIslandsParallelGeneticAlgorithmMonitor Monitor { get; set; }
    }
}