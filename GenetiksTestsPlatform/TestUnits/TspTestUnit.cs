﻿using Genetiks.Factories.IslandFactories;

namespace GenetiksTestsPlatform.TestUnits
{
    public class TspTestUnit : TestUnit
    {
        public double[,] Map { get; set; }
    }
}