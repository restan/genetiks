﻿using System.Collections.Generic;
using KnapsackProblem;

namespace GenetiksTestsPlatform.TestUnits
{
    public class KnapsackTestUnit : TestUnit
    {
        public IList<IKnapsackItem> Items { get; set; }
        public IKnapsack Knapsack { get; set; }

    }
}