﻿using System;

namespace TSP.Map
{
    public class MapGenerator
    {
        public double[,] GenerateMap(int citiesCount)
        {
            Random rand = new Random((int)DateTime.Now.Ticks);

            // create coordinates array
            var map = new double[citiesCount, 2];

            for (int i = 0; i < citiesCount; i++)
            {
                map[i, 0] = rand.Next(1001);
                map[i, 1] = rand.Next(1001);
            }

            return map;
        }

    }
}
