﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TSP.Map
{
    public class MapPersister
    {
        public void SaveMap(double[,] map, string fileName)
        {
            var fs = File.Create(fileName);
            var bf = new BinaryFormatter();

            bf.Serialize(fs, map);

            fs.Close();

        }

        public double[,] LoadMap(string fileName)
        {
            var fs = File.Open(fileName, FileMode.Open);
            var bf = new BinaryFormatter();
            var map = (double[,])bf.Deserialize(fs);

            return map;
        }
    }
}