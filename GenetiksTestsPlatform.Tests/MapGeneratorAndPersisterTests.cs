﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSP.Map;

namespace GenetiksTestsPlatform.Tests
{
    [TestClass]
    public class MapGeneratorAndPersisterTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var mapGenerator = new MapGenerator();
            var persister = new MapPersister();

            var map = mapGenerator.GenerateMap(1000);

            persister.SaveMap(map, "testMap");

            var map1 = persister.LoadMap("testMap");

            CollectionAssert.AreEquivalent(map, map1);
        }
    }
}
