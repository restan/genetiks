﻿using System;
using System.Collections;
using KnapsackProblem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GenetiksTestsPlatform.Tests
{
    [TestClass]
    public class VariousTests
    {

        [TestMethod]
        public void ByteTests()
        {
            var value = (byte)5;

            Assert.IsTrue(value.GetBit(0));
            Assert.IsFalse(value.GetBit(1));
            Assert.IsTrue(value.GetBit(2));

        }

        [TestMethod]
        public void GenerateByteArrayTest()
        {
            var length = 10;
            var bitArray = new BitArray(length);

            for (var i = 0; i < length; i++)
            {
                if (i % 2 == 0)
                    bitArray[i] = true;
            }

            var bytes = ToByteArray(bitArray);


            var val = BitConverter.ToUInt64(bytes, 0);

            Console.WriteLine(val);
        }
        private static byte[] ToByteArray(BitArray bits)
        {
            byte[] bytes = new byte[8];
            int byteIndex = 0, bitIndex = 0;

            for (int i = 0; i < bits.Count; i++)
            {
                bytes[byteIndex] = (byte)(bytes[byteIndex] << 1);

                if (bits[i])
                    bytes[byteIndex] |= 1;

                bitIndex++;
                if (bitIndex == 8)
                {
                    bitIndex = 0;
                    byteIndex++;
                }
            }

            return bytes;
        }

    }
}