﻿using System;
using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;
using Genetiks;
using Genetiks.Factories.IslandFactories;
using Genetiks.Factories.SelectionMethods;
using Genetiks.MigrationStrategies;
using GenetiksTestsPlatform.FileConfiguration;
using GenetiksTestsPlatform.Testers;
using KnapsackProblem;
using KnapsackProblem.DataFileParsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GenetiksTestsPlatform.Tests
{
    [TestClass]
    public class KnapsackProblemTests
    {
        [TestMethod]
        [DeploymentItem("KnapsackTestData\\p08prim")]
        public void TesterTests()
        {
            var dataFileName = "p08prim";

            var tester = new KnapsackTester();
            var configuration = new Configuration
                {
                    CrossoverRates = new[] { 0.75 },
                    MutationRates = new[] { 0.1 },
                    IslandNumbers = new[] { 2, 5 },
                    Iterations = new[] { 1000 },
                    MigrationFrequencies = new[] { 5 },
                    PopulationSizes = new[] { 100, 500 },
                    SelectionMethods = new List<ESelectionMethod> { ESelectionMethod.EliteSelection },
                    MigrationStrategies = new List<EMigrationStrategy>
                        {
                            EMigrationStrategy.SimplexRingMigrationStrategy,
                        },
                    DataFile = dataFileName,
                    Repetitions = 10,
                    MigrationSelectionStrategies = new List<EIslandFactory> { EIslandFactory.RandomMigration },
                    RandomFactors = new double[] { 0.05, 0.10 },
                };

            var results = tester.Test(configuration).ToList();

            var resultPrinter = new TestResultPrinter();
            resultPrinter.Print("d:\\KnapsackTestResults", results);
        }
        [TestMethod]
        [DeploymentItem("KnapsackTestData\\p08")]
        public void P08Tests()
        {
            var parser = new KnapsackDataFileParser();
            var data = parser.ParseFile("p08");
            var knapsack = new Knapsack { Capacity = data.Capacity };

            var fitnessFunction = new KnapsackFitnessFunction(knapsack, data.Items);

            IChromosome ancestorChromosome = new KnapsackChromosome(data.Items, knapsack);
            IAncestorPopulation ancestorPopulation = new AncestorPopulation(ancestorChromosome, fitnessFunction);
            IMigrationStrategy migrationStrategy = new SimplexRingMigrationStrategy();
            var paralelAlgotirhm = new IslandsParallelGeneticAlgorithm(ancestorPopulation, migrationStrategy, 1, new RandomMigrationIslandFactory(0.05));

            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());
            paralelAlgotirhm.AddPopulation(100, new EliteSelection());

            paralelAlgotirhm.Compute(10000);
            var result = (BinaryChromosome)paralelAlgotirhm.BestChromosome;

            Console.WriteLine(result);
            Console.WriteLine("Result  {0}", result.Fitness);

            Console.WriteLine("Optimal {0}", data.OptimalValue);
        }


    }
}