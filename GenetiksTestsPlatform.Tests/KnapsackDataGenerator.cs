﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using KnapsackProblem;
using KnapsackProblem.DataFileParsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GenetiksTestsPlatform.Tests
{
    public static class RandomExtensions
    {
        // Return a random value between 0 inclusive and max exclusive.
        public static double NextDouble(this Random rand, double max)
        {
            return rand.NextDouble() * max;
        }

        // Return a random value between min inclusive and max exclusive.
        public static double NextDouble(this Random rand, double min, double max)
        {
            return min + (rand.NextDouble() * (max - min));
        }
    }
    [TestClass]
    public class KnapsackDataGenerator
    {

        [TestMethod]
        [DeploymentItem("KnapsackTestData\\p08")]
        public void Generate()
        {
            var parser = new KnapsackDataFileParser();
            var data = parser.ParseFile("p08");

            var minValue = data.Items.Select(x => x.Value).Min();
            var maxValue = data.Items.Select(x => x.Value).Max();

            var minWeights = data.Items.Select(x => x.Weight).Min();
            var maxWeights = data.Items.Select(x => x.Weight).Max();


            var items = new List<IKnapsackItem>();
            var random = new Random();
            for (var i = 0; i < 64; i++)
            {
                var item = new KnapsackItem((int)random.NextDouble(minValue, maxValue),
                                            (int)random.NextDouble(minWeights, maxWeights));

                items.Add(item);
            }

            var sb = new StringBuilder();
            sb.AppendLine((data.Capacity * 3).ToString());
            sb.AppendLine("0");
            foreach (var item in items)
                sb.AppendLine(String.Format("{0} {1}", item.Value, item.Weight));

            File.WriteAllText("D:\\p08prim", sb.ToString());


        }
    }
}