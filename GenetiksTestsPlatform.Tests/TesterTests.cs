﻿using System.Collections.Generic;
using System.Linq;
using Genetiks.Factories.SelectionMethods;
using Genetiks.MigrationStrategies;
using GenetiksTestsPlatform.FileConfiguration;
using GenetiksTestsPlatform.Testers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSP.Map;

namespace GenetiksTestsPlatform.Tests
{
    [TestClass]
    public class TesterTests
    {
        [TestMethod]
        public void Test()
        {
            var mapFileName = "testMapFile";

            var mapGenerator = new MapGenerator();
            var persister = new MapPersister();
            var map = mapGenerator.GenerateMap(1000);
            persister.SaveMap(map, mapFileName);


            var configuration = new Configuration
            {
                CrossoverRates = new[] { 0.75 },
                MutationRates = new[] { 0.1 },
                IslandNumbers = new[] { 10 },
                Iterations = new[] { 500 },
                MigrationFrequencies = new[] { 10 },
                PopulationSizes = new[] { 100 },
                SelectionMethods = new List<ESelectionMethod> { ESelectionMethod.EliteSelection },
                MigrationStrategies = new List<EMigrationStrategy>
                            {
                            EMigrationStrategy.DuplexRingMigrationStrategy,
                            EMigrationStrategy.LadderMigrationStrategy,
                            EMigrationStrategy.NoMigrationStrategy,
                            EMigrationStrategy.RingOnePlusTwoMigrationStrategy,
                            EMigrationStrategy.RingThreePlusFourMigrationStrategy,
                            EMigrationStrategy.SimplexRingMigrationStrategy
                            },
                DataFile = mapFileName,
                Repetitions = 10
            };


            var tester = new TspTester();
            var results = tester.Test(configuration).ToList();

            var resultsPrinter = new TestResultPrinter();
            resultsPrinter.Print("d:\\results.csv", results);

        }
    }
}
