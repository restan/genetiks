using System.Collections.Generic;

namespace Genetiks.Monitor
{
    public class IslandMonitor : IIslandMonitor
    {
        private readonly IList<IslandMonitorData> _data = new List<IslandMonitorData>();
        public IList<IslandMonitorData> Data
        {
            get { return _data; }
        }

        public void TraceData(IslandMonitorData islandMonitorData)
        {
            Data.Add(islandMonitorData);
        }

    }
}