using System.Collections.Generic;

namespace Genetiks.Monitor
{
    public interface IIslandsParallelGeneticAlgorithmMonitor
    {
        IIslandMonitor CreateIslandMonitor();
        IEnumerable<IEnumerable<IslandMonitorData>> AllData { get; }
        IEnumerable<IslandMonitorData> Data { get; }
    }
}