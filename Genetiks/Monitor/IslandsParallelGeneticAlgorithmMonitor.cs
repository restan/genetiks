using System;
using System.Collections.Generic;
using System.Linq;

namespace Genetiks.Monitor
{
    public class IslandsParallelGeneticAlgorithmMonitor : IIslandsParallelGeneticAlgorithmMonitor
    {
        private readonly IList<IIslandMonitor> _monitors = new List<IIslandMonitor>();
        public Guid Guid { get; private set; }

        public IslandsParallelGeneticAlgorithmMonitor()
        {
            Guid = Guid.NewGuid();
        }

        public IslandsParallelGeneticAlgorithmMonitor(Guid guid)
        {
            Guid = guid;
        }

        public IIslandMonitor CreateIslandMonitor()
        {
            var islandMonitor = new IslandMonitor();
            _monitors.Add(islandMonitor);
            return islandMonitor;
        }

        public IEnumerable<IEnumerable<IslandMonitorData>> AllData
        {
            get { return _monitors.Select(m => m.Data); }
        }

        public IEnumerable<IslandMonitorData> Data
        {
            get
            {
                var computedData = new List<IslandMonitorData>();
                var iterationsNumber = _monitors[0].Data.Count();

                for (var i = 0; i < iterationsNumber; i++)
                {
                    computedData.Add(new IslandMonitorData
                        {
                            IterationNumber = i,
                            BestCrhomosomeFitness = _monitors.Select(m => m.Data[i].BestCrhomosomeFitness).Max()
                        });
                }

                return computedData;
            }
        }
    }
}