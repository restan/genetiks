namespace Genetiks.Monitor
{
    public class IslandMonitorData
    {
        public int IterationNumber { get; set; }
        public double BestCrhomosomeFitness { get; set; }
    }
}