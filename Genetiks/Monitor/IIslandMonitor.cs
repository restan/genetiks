using System.Collections.Generic;

namespace Genetiks.Monitor
{
    public interface IIslandMonitor
    {
        void TraceData(IslandMonitorData islandMonitorData);
        IList<IslandMonitorData> Data { get; }
    }
}