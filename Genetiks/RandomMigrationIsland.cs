﻿using System;
using System.Collections.Generic;
using AForge.Genetic;
using Genetiks.MigrationStrategies;

namespace Genetiks
{
    public class RandomMigrationIsland : BaseIsland
    {
        private readonly double _randomFactor;
        private readonly Random _random = new Random();
        private readonly ISet<IChromosome> _immigrants = new HashSet<IChromosome>();
        private readonly Object _lock = new object();

        public RandomMigrationIsland(Population population, IMigrationStrategy migrationStrategy, int migrationFrequency, double randomFactor)
            : base(population, migrationStrategy, migrationFrequency)
        {
            _randomFactor = randomFactor;
        }

        public override void AcceptImmigrant(IChromosome chromosome)
        {
            lock (_lock)
            {
                _immigrants.Add(chromosome);
            }
        }

        protected override void AddImmigrantsToPopulation()
        {
            var populationSize = _population.Size;
            lock (_lock)
            {

                foreach (var immigrant in _immigrants)
                {
                    _population.AddChromosome(immigrant);
                    _population.Resize(_population.Size + 1);
                }
                _immigrants.Clear();
            }
            _population.Resize(populationSize);
        }

        protected override IList<IChromosome> GetChromosomesToMigrate(Population population)
        {
            var chromosomes = new List<IChromosome>();

            for (var i = 0; i < population.Size; i++)
            {
                var randomValue = _random.NextDouble();
                if (randomValue < _randomFactor)
                    chromosomes.Add(population[i]);
            }
            return chromosomes;
        }
    }
}