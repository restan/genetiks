﻿using AForge.Genetic;

namespace Genetiks
{
    public interface IAncestorPopulation
    {
        IChromosome AncestorChromosome { get; }
        IFitnessFunction FitnessFunction { get; }
    }
}