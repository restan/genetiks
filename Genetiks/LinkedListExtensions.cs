﻿using System.Collections.Generic;

namespace Genetiks
{
    public static class LinkedListExtensions
    {
        public static LinkedListNode<T> NextOrFirst<T>(this LinkedListNode<T> linkedListNode)
        {
            if (linkedListNode.Next != null)
                return linkedListNode.Next;

            return linkedListNode.List.First;
        }
        public static LinkedListNode<T> PreviousOrLast<T>(this LinkedListNode<T> linkedListNode)
        {
            if (linkedListNode.Previous != null)
                return linkedListNode.Previous;

            return linkedListNode.List.Last;
        }
    }
}