﻿using System.Threading.Tasks;
using AForge.Genetic;
using Genetiks.Monitor;

namespace Genetiks
{
    public interface IIsland
    {
        void AcceptImmigrant(IChromosome chromosome);
        IChromosome BestChromosome { get; }
        Task Run(int iterations, IIslandMonitor monitor);
        Task Run(int iterationsNumber);
    }
}