﻿using AForge.Genetic;
using Genetiks.MigrationStrategies;

namespace Genetiks.Factories.IslandFactories
{
    public class RandomMigrationIslandFactory : IIslandFactory
    {
        private readonly double _randomFactor;

        public RandomMigrationIslandFactory(double randomFactor)
        {
            _randomFactor = randomFactor;
        }

        public IIsland Create(Population population, IMigrationStrategy migrationStrategy, int migrationFrequency)
        {
            return new RandomMigrationIsland(population, migrationStrategy, migrationFrequency, _randomFactor);

        }
    }
}