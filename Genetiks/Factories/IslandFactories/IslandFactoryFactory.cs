﻿using System;
using System.Collections.Generic;

namespace Genetiks.Factories.IslandFactories
{
    public class IslandFactoryFactory : IIslandFactoryFactory
    {
        public IEnumerable<IIslandFactory> Create(IEnumerable<EIslandFactory> islandFactories, IEnumerable<double> parameters)
        {
            foreach (var islandFactory in islandFactories)
            {
                switch (islandFactory)
                {
                    case EIslandFactory.BestMigration:
                        yield return new BestMigrationIslandFactory();
                        break;
                    case EIslandFactory.RandomMigration:
                        foreach (var randomFactor in parameters)
                            yield return new RandomMigrationIslandFactory(randomFactor);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("islandFactory");
                }
            }
        }
    }
}