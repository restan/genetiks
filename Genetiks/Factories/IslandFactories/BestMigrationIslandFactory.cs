﻿using AForge.Genetic;
using Genetiks.MigrationStrategies;

namespace Genetiks.Factories.IslandFactories
{
    public class BestMigrationIslandFactory : IIslandFactory
    {
        public IIsland Create(Population population, IMigrationStrategy migrationStrategy, int migrationFrequency)
        {
            return new BestMigrationInsland(population, migrationStrategy, migrationFrequency);
        }
    }
}