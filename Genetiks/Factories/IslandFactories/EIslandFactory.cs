﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetiks.Factories.IslandFactories
{
    public enum EIslandFactory
    {
        BestMigration,
        RandomMigration
    }
}
