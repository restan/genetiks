﻿using AForge.Genetic;
using Genetiks.MigrationStrategies;

namespace Genetiks.Factories.IslandFactories
{
    public interface IIslandFactory
    {
        IIsland Create(Population population, IMigrationStrategy migrationStrategy, int migrationFrequency);
    }
}