﻿using System.Collections.Generic;

namespace Genetiks.Factories.IslandFactories
{
    public interface IIslandFactoryFactory
    {
         IEnumerable<IIslandFactory> Create(IEnumerable<EIslandFactory> islandFactory, IEnumerable<double> parameters);
    }
}