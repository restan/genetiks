﻿using AForge.Genetic;

namespace Genetiks.Factories.SelectionMethods
{
    public interface ISelectionMethodFactory
    {
        ISelectionMethod Create(ESelectionMethod selectionMethod);
    }
}