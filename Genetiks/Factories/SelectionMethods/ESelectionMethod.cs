﻿namespace Genetiks.Factories.SelectionMethods
{
    public enum ESelectionMethod
    {
        EliteSelection,
        RankSelection,
        RouletteWheelSelection
    }
}