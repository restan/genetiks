﻿using System;
using AForge.Genetic;

namespace Genetiks.Factories.SelectionMethods
{
    public class SelectionMethodFactory : ISelectionMethodFactory
    {
        public ISelectionMethod Create(ESelectionMethod selectionMethod)
        {
            switch (selectionMethod)
            {
                case ESelectionMethod.EliteSelection:
                    return new EliteSelection();
                case ESelectionMethod.RankSelection:
                    return new RankSelection();
                case ESelectionMethod.RouletteWheelSelection:
                    return new RouletteWheelSelection();
                default:
                    throw new ArgumentOutOfRangeException("selectionMethod");
            }
        }
    }
}