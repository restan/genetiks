﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AForge.Genetic;
using Genetiks.Factories.IslandFactories;
using Genetiks.MigrationStrategies;
using Genetiks.Monitor;

namespace Genetiks
{
  
    public class IslandsParallelGeneticAlgorithm : IParallelGeneticAlgorithm
    {
        private readonly IAncestorPopulation _ancestorPopulation;
        private readonly IMigrationStrategy _migrationStrategy;
        private readonly int _migrationFrequency;
        private readonly IIslandFactory _islandFactory;
        private readonly IList<Population> _populations;
        private IList<IIsland> _islands;

        public IslandsParallelGeneticAlgorithm(IAncestorPopulation ancestorPopulation, IMigrationStrategy migrationStrategy, int migrationFrequency, IIslandFactory islandFactory)
        {
            _migrationStrategy = migrationStrategy;
            _migrationFrequency = migrationFrequency;
            _islandFactory = islandFactory;
            _ancestorPopulation = ancestorPopulation;
            _populations = new List<Population>();
        }


        public void AddPopulation(int size, ISelectionMethod selectionMethod)
        {
            var population = new Population(size, _ancestorPopulation.AncestorChromosome,
                                        _ancestorPopulation.FitnessFunction,
                                        selectionMethod);

            _populations.Add(population);
        }

        public void AddPopulation(int size, ISelectionMethod selectionMethod, double crossoverRate, double mutationRate)
        {
            var population = new Population(size, _ancestorPopulation.AncestorChromosome,
                                            _ancestorPopulation.FitnessFunction,
                                            selectionMethod);

            population.CrossoverRate = crossoverRate;
            population.MutationRate = mutationRate;

            _populations.Add(population);
        }

        public void Compute(int iterationsNumber)
        {
            Initialize();
            var tasks = _islands.Select(island => island.Run(iterationsNumber)).ToArray();
            Task.WaitAll(tasks);
        }

        public void Compute(int iterationsNumber, IIslandsParallelGeneticAlgorithmMonitor monitor)
        {
            Initialize();
            var tasks = _islands.Select(island => island.Run(iterationsNumber, monitor.CreateIslandMonitor())).ToArray();
            Task.WaitAll(tasks);
        }

        private void Initialize()
        {
            _islands = _populations.Select(CreateIsland).ToList();
            _migrationStrategy.Initialize(_islands);
        }
        private IIsland CreateIsland(Population population)
        {
            return _islandFactory.Create(population, _migrationStrategy, _migrationFrequency);
        }

        public IChromosome BestChromosome
        {
            get { return _islands.Select(x => x.BestChromosome).OrderBy(chromosome => chromosome).First(); }
        }

    }
}