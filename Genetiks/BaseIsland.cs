﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AForge.Genetic;
using Genetiks.MigrationStrategies;
using Genetiks.Monitor;

namespace Genetiks
{
    public abstract class BaseIsland : IIsland
    {
        protected readonly Population _population;
        protected readonly IMigrationStrategy _migrationStrategy;
        private readonly int _migrationFrequency;

        protected IList<IIsland> DestinationIslands
        {
            get { return _migrationStrategy.GetDestinationIslands(this); }
        }

        public BaseIsland(Population population, IMigrationStrategy migrationStrategy, int migrationFrequency)
        {
            _population = population;
            _migrationStrategy = migrationStrategy;
            _migrationFrequency = migrationFrequency;
        }

        public abstract void AcceptImmigrant(IChromosome chromosome);

        public IChromosome BestChromosome
        {
            get { return _population.BestChromosome; }
        }


        public Task Run(int iterationsNumber)
        {
            return Task.Run(() =>
            {
                for (var i = 0; i < iterationsNumber; i++)
                {
                    //migracja nie przy pierwszej iteracji
                    if (i > 0 && i % _migrationFrequency == 0)
                        Migrate();

                    _population.RunEpoch();
                }
            });
        }


        public Task Run(int iterationsNumber, IIslandMonitor monitor)
        {
            return Task.Run(() =>
            {
                for (var i = 0; i < iterationsNumber; i++)
                {
                    //migracja nie przy pierwszej iteracji
                    if (i > 0 && i % _migrationFrequency == 0)
                        Migrate();

                    _population.RunEpoch();
                    monitor.TraceData(new IslandMonitorData
                        {
                            IterationNumber = i,
                            BestCrhomosomeFitness = _population.BestChromosome.Fitness
                        });
                }
            });
        }



        private void Migrate()
        {
            SendChromosomes();
            AddImmigrantsToPopulation();
        }

        protected abstract void AddImmigrantsToPopulation();

        private void SendChromosomes()
        {
            foreach (var island in DestinationIslands)
            {
                var chromosomesToSend = GetChromosomesToMigrate(_population);

                foreach (var chromosome in chromosomesToSend)
                    island.AcceptImmigrant(chromosome);
            }

        }

        protected abstract IList<IChromosome> GetChromosomesToMigrate(Population population);
    }
}
