﻿using System.Collections.Generic;

namespace Genetiks.MigrationStrategies
{
    public class NoMigrationStrategy : BaseMigrationStrategy
    {
        public override int MigratingChromosomesNumber { get { return 0; } }

        public override void Initialize(IList<IIsland> islands)
        {
            foreach (var island in islands)
                DestinationIslands[island] = new List<IIsland>();
        }
    }
}