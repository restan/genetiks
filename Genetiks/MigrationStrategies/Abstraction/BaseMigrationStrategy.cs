﻿using System;
using System.Collections.Generic;

namespace Genetiks.MigrationStrategies
{
    public abstract class BaseMigrationStrategy : IMigrationStrategy
    {
        private readonly IDictionary<IIsland, IList<IIsland>> _destinationIslands = new Dictionary<IIsland, IList<IIsland>>();
        protected IDictionary<IIsland, IList<IIsland>> DestinationIslands
        {
            get { return _destinationIslands; }
        }

        public IList<IIsland> GetDestinationIslands(IIsland island)
        {
            var islands = DestinationIslands[island];
            if (islands.Count != MigratingChromosomesNumber)
                throw new Exception("Migration strategy is not consistent");

            return islands;
        }

        public abstract int MigratingChromosomesNumber { get; }
        public abstract void Initialize(IList<IIsland> islands);

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}