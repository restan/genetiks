﻿using System.Collections.Generic;

namespace Genetiks.MigrationStrategies
{
    public interface IMigrationStrategy
    {
        int MigratingChromosomesNumber { get; }
        void Initialize(IList<IIsland> islands);
        IList<IIsland> GetDestinationIslands(IIsland island);
    }
}