﻿using System;
using System.Collections.Generic;
using Genetiks.MigrationStrategies.LadderMigration;

namespace Genetiks.MigrationStrategies
{
    public class LadderMigrationStrategy : BaseMigrationStrategy
    {
        public override int MigratingChromosomesNumber
        {
            get { return 2; }
        }

        public override void Initialize(IList<IIsland> islands)
        {
            if (islands.Count < 4 || islands.Count % 2 != 0)
                throw new Exception("Populations number is not valid");

            var islandsLadderFactory = new IslandsLadderFactory();
            var ladder = islandsLadderFactory.CreateLadder(islands);

            for (var rungNode = ladder.First; rungNode != null; rungNode = rungNode.Next)
            {
                var nextRungNode = rungNode.NextOrFirst();

                DestinationIslands[rungNode.Value.Left] = new List<IIsland>
                    {
                        rungNode.Value.Right,
                        nextRungNode.Value.Left
                    };

                DestinationIslands[rungNode.Value.Right] = new List<IIsland>
                    {
                        rungNode.Value.Left,
                        nextRungNode.Value.Right
                    };
            }
        }
    }
}