﻿using System;

namespace Genetiks.MigrationStrategies.Factory
{
    public class MigrationStrategyFactory : IMigrationStrategyFactory
    {
        public IMigrationStrategy Create(EMigrationStrategy migrationStrategy)
        {
            switch (migrationStrategy)
            {
                case EMigrationStrategy.SimplexRingMigrationStrategy:
                    return new SimplexRingMigrationStrategy();
                case EMigrationStrategy.DuplexRingMigrationStrategy:
                    return new DuplexRingMigrationStrategy();
                case EMigrationStrategy.LadderMigrationStrategy:
                    return new LadderMigrationStrategy();
                case EMigrationStrategy.NoMigrationStrategy:
                    return new NoMigrationStrategy();
                case EMigrationStrategy.RingOnePlusTwoMigrationStrategy:
                    return new RingOnePlusTwoMigrationStrategy();
                case EMigrationStrategy.RingThreePlusFourMigrationStrategy:
                    return new RingThreePlusFourMigrationStrategy();
                default:
                    throw new ArgumentOutOfRangeException("migrationStrategy");
            }
        }
    }
}