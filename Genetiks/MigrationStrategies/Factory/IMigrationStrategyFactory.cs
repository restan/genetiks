﻿namespace Genetiks.MigrationStrategies.Factory
{
    public interface IMigrationStrategyFactory
    {
        IMigrationStrategy Create(EMigrationStrategy migrationStrategy);
    }
}