﻿using System;
using System.Collections.Generic;

namespace Genetiks.MigrationStrategies.LadderMigration
{
    class IslandsLadderFactory
    {
        public Ladder<IIsland> CreateLadder(IList<IIsland> islands)
        {
            if (islands.Count < 4 || islands.Count % 2 != 0)
                throw new Exception("Islands number is not valid to create ladder");

            var islandsLinkedList = new LinkedList<IIsland>(islands);

            var ladder = new Ladder<IIsland>();
            for (var islandNode = islandsLinkedList.First;
                islandNode != null && islandNode.Next != null;
                islandNode = islandNode.Next.Next)
            {
                var rung = new Rung<IIsland>
                    {
                        Left = islandNode.Value,
                        Right = islandNode.Next.Value
                    };
                ladder.AddLast(rung);
            }

            return ladder;
        }
    }
}