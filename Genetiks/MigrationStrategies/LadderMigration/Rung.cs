﻿namespace Genetiks.MigrationStrategies.LadderMigration
{
    class Rung<T>
    {
        public T Left { get; set; }
        public T Right { get; set; }
    }
}