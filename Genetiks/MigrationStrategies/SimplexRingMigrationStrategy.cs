﻿using System;
using System.Collections.Generic;

namespace Genetiks.MigrationStrategies
{
    public class SimplexRingMigrationStrategy : BaseMigrationStrategy
    {
        public override int MigratingChromosomesNumber
        {
            get { return 1; }
        }

        public override void Initialize(IList<IIsland> islands)
        {
            if (islands.Count < 2)
                throw new Exception("At least 2 populations needed;");

            var islandsLinkedList = new LinkedList<IIsland>(islands);
            for (var islandNode = islandsLinkedList.First; islandNode != null; islandNode = islandNode.Next)
            {
                var destinationIslands = new List<IIsland>();
                DestinationIslands.Add(islandNode.Value, destinationIslands);

                var nextNode = islandNode.NextOrFirst();
                destinationIslands.Add(nextNode.Value);
            }
        }
    }
}