﻿namespace Genetiks.MigrationStrategies
{
    public enum EMigrationStrategy
    {
        SimplexRingMigrationStrategy,
        DuplexRingMigrationStrategy,
        LadderMigrationStrategy,
        NoMigrationStrategy,
        RingOnePlusTwoMigrationStrategy,
        RingThreePlusFourMigrationStrategy
    }
}