﻿using AForge.Genetic;

namespace Genetiks
{
    public class AncestorPopulation : IAncestorPopulation
    {
        public IChromosome AncestorChromosome { get; private set; }
        public IFitnessFunction FitnessFunction { get; private set; }

        public AncestorPopulation(IChromosome ancestorChromosome, IFitnessFunction fitnessFunction)
        {
            AncestorChromosome = ancestorChromosome;
            FitnessFunction = fitnessFunction;
        }
    }
}