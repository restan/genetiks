﻿using AForge.Genetic;
using Genetiks.Monitor;

namespace Genetiks
{
    public interface IParallelGeneticAlgorithm
    {
        void AddPopulation(int size, ISelectionMethod selectionMethod, double crossoverRate, double mutationRate);
        void AddPopulation(int size, ISelectionMethod selectionMethod);
        void Compute(int iterationsNumber);
        IChromosome BestChromosome { get; }
        void Compute(int iterationsNumber, IIslandsParallelGeneticAlgorithmMonitor monitor);
    }
}
