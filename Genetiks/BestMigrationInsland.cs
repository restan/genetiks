﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using AForge.Genetic;
using Genetiks.MigrationStrategies;

namespace Genetiks
{
    public class BestMigrationInsland : BaseIsland
    {
        private readonly BlockingCollection<IChromosome> _immigrants = new BlockingCollection<IChromosome>();
        private int MigratingChromosomesNumber { get { return _migrationStrategy.MigratingChromosomesNumber; } }

        public BestMigrationInsland(Population population, IMigrationStrategy migrationStrategy, int migrationFrequency)
            : base(population, migrationStrategy, migrationFrequency)
        {
        }

        public override void AcceptImmigrant(IChromosome chromosome)
        {
            _immigrants.Add(chromosome);
        }

        protected override void AddImmigrantsToPopulation()
        {
            for (var i = 0; i < MigratingChromosomesNumber; i++)
            {
                var chromosome = _immigrants.Take();
                _population.AddChromosome(chromosome);
                _population.Resize(_population.Size + 1);
            }
            _population.Resize(_population.Size - MigratingChromosomesNumber);
        }

        protected override IList<IChromosome> GetChromosomesToMigrate(Population population)
        {
            return new List<IChromosome> { population.BestChromosome.Clone() };
        }
    }
}