﻿using System;
using System.Runtime.Serialization;

namespace KnapsackProblem
{
    public class KnapsackOverweightException : Exception
    {
        public KnapsackOverweightException()
        {
        }

        public KnapsackOverweightException(string message) : base(message)
        {
        }

        public KnapsackOverweightException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected KnapsackOverweightException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}