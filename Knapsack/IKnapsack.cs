﻿namespace KnapsackProblem
{
    public interface IKnapsack
    {
        double Capacity { get; }
    }
}