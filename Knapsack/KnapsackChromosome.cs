﻿using System;
using System.Collections;
using System.Collections.Generic;
using AForge.Genetic;

namespace KnapsackProblem
{
    public class KnapsackChromosome : BinaryChromosome
    {
        private readonly IList<IKnapsackItem> _items;
        private readonly IKnapsack _knapsack;

        public KnapsackChromosome(IList<IKnapsackItem> items, IKnapsack knapsack)
            : base(items.Count)
        {
            _items = items;
            _knapsack = knapsack;
            Generate();
        }

        protected KnapsackChromosome(KnapsackChromosome source)
            : base(source)
        {
            _items = source._items;
            _knapsack = source._knapsack;
        }

        public override IChromosome CreateNew()
        {
            return new KnapsackChromosome(_items, _knapsack);
        }
        public override IChromosome Clone()
        {
            return new KnapsackChromosome(this);
        }

        //public override void Crossover(IChromosome pair)
        //{
        //    KnapsackChromosome p = (KnapsackChromosome)pair;
        //    if ((p != null) && (p.length == length))
        //    {
        //        UInt64 mask = 1;
        //        for (int i = 0; i < p.length; i++)
        //        {
        //            if ((val & mask) != (p.val & mask))
        //            {
        //                if (rand.Next(0, 2) == 1)
        //                    val = val | mask;
        //                else
        //                    val = val & ~mask;
        //                if (rand.Next(0, 2) == 1)
        //                    p.val = p.val | mask;
        //                else
        //                    p.val = p.val & ~mask;
        //            }
        //            mask = mask << 1;
        //        }
        //    }
        //}

        public override void Generate()
        {
            //if (_items == null)
            //    return;

            //var totalWeights = 0.0;
            //var bitArray = new BitArray(length);

            //while (true)
            //{
            //    var index = rand.Next(0, length);
            //    if (bitArray[index])
            //        continue;

            //    var itemWeight = _items[index].Weight;
            //    if (totalWeights + itemWeight > _knapsack.Capacity)
            //        break;

            //    bitArray[index] = true;
            //    totalWeights += itemWeight;
            //}

            //val = BitConverter.ToUInt64(ToByteArray(bitArray), 0);
            val = 0;
        }

        private static byte[] ToByteArray(BitArray bits)
        {
            byte[] bytes = new byte[8];
            int byteIndex = 0, bitIndex = 0;

            for (int i = 0; i < bits.Count; i++)
            {
                bytes[byteIndex] = (byte)(bytes[byteIndex] << 1);

                if (bits[i])
                    bytes[byteIndex] |= 1;

                bitIndex++;
                if (bitIndex == 8)
                {
                    bitIndex = 0;
                    byteIndex++;
                }
            }

            return bytes;
        }

    }
}
