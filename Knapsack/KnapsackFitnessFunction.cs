﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;

namespace KnapsackProblem
{
    public class KnapsackFitnessFunction : IFitnessFunction
    {
        private readonly IKnapsack _knapsack;
        private readonly IList<IKnapsackItem> _items;
        public KnapsackFitnessFunction(IKnapsack knapsack, IList<IKnapsackItem> items)
        {
            _knapsack = knapsack;
            _items = items;
        }

        public double Evaluate(IChromosome chromosome)
        {
            return CalculateItemsValue(chromosome);
        }

        public double CalculateItemsValue(IChromosome chromosome)
        {
            var binaryChromosome = (BinaryChromosome)chromosome;
            var itemsInChromosome = GetItemsInChromosome(binaryChromosome).ToList();
            var itemsWeight = itemsInChromosome.Select(x => x.Weight).Sum();
            if (itemsWeight > _knapsack.Capacity)
                return double.MinValue;

            var itemsValue = itemsInChromosome.Select(x => x.Value).Sum();
            return itemsValue;
        }
        private IEnumerable<IKnapsackItem> GetItemsInChromosome(BinaryChromosome binaryChromosome)
        {
            var value = binaryChromosome.Value;
            var bitArray = new BitArray(BitConverter.GetBytes(value));
            for (var i = 0; i < binaryChromosome.Length; i++)
            {
                if (bitArray[i])
                    yield return _items[i];
            }
        }
    }
}
