﻿namespace KnapsackProblem
{
    public class KnapsackItem : IKnapsackItem
    {
        public double Value { get; set; }
        public double Weight { get; set; }

        public KnapsackItem(double value, double weight)
        {
            Value = value;
            Weight = weight;
        }

        public KnapsackItem()
        {
        }
    }
}