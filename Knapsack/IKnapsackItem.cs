﻿namespace KnapsackProblem
{
    public interface IKnapsackItem
    {
        double Value { get; set; }
        double Weight { get; set; }
    }
}