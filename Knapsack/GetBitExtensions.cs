﻿using System;
using System.Collections;

namespace KnapsackProblem
{
    public static class GetBitExtensions
    {
        public static bool GetBit(this byte b, int bitNumber)
        {
            return (b & (1 << bitNumber)) != 0;
        }

        public static bool GetBit(this ulong value, int bitNumber)
        {
            var bitArray = new BitArray(BitConverter.GetBytes(value));
            return bitArray[bitNumber];
        }
    }
}
