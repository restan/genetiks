﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.DataFileParsers
{
    public class KnapsackDataFileParser
    {
        public KnapsackDataFromFile ParseFile(string fileName)
        {
            var lines = File.ReadAllLines(fileName);
            var capacity = double.Parse(lines[0]);
            var optimalValue = double.Parse(lines[1]);

            var items = new List<IKnapsackItem>();
            for (var i = 2; i < lines.Length; i++)
            {
                var splittedLine = lines[i].Split(' ');

                items.Add(new KnapsackItem
                {
                    Value = double.Parse(splittedLine[0]),
                    Weight = double.Parse(splittedLine[1])
                });
            }

            return new KnapsackDataFromFile
            {
                Capacity = capacity,
                Items = items,
                OptimalValue = optimalValue
            };
        }
    }
}
