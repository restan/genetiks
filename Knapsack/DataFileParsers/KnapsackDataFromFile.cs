﻿using System.Collections.Generic;

namespace KnapsackProblem.DataFileParsers
{
    public class KnapsackDataFromFile
    {
        public double Capacity { get; set; }
        public double OptimalValue { get; set; }
        public IList<IKnapsackItem> Items { get; set; }
    }
}